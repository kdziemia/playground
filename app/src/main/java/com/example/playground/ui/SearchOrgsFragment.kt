package com.example.playground.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.coroutineScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.playground.R
import com.example.playground.data.github.GitHubAPI
import com.example.playground.data.github.GithubRepository
import com.example.playground.data.github.SearchOrgsUseCase
import com.example.playground.databinding.FragmentSearchOrgsBinding
import com.example.playground.databinding.ItemOrgBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SearchOrgsFragment : Fragment() {
    lateinit var model: SearchOrgsViewModel
    lateinit var binding: FragmentSearchOrgsBinding

    @Inject lateinit var useCase: SearchOrgsUseCase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSearchOrgsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        model = ViewModelProvider(this, Factory(useCase)).get(SearchOrgsViewModel::class.java)

        binding.recyclerView.adapter = MyAdapter()
        binding.searchField.setOnEditorActionListener { p0, p1, p2 ->
            when (p1) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    model.onTermEntered(p0?.text.toString())
                }
            }
            false
        }

        viewLifecycleOwner.lifecycle.coroutineScope.launchWhenResumed {
            model.items.collect {
                (binding.recyclerView.adapter as MyAdapter).notifyDataSetChanged()
            }
        }
    }

    inner class MyAdapter: RecyclerView.Adapter<OrgHolder>() {

        override fun getItemCount(): Int {
            return model.items.value.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrgHolder {
            return OrgHolder(ItemOrgBinding.inflate(layoutInflater, parent, false))
        }

        override fun onBindViewHolder(holder: OrgHolder, position: Int) {
            model.items.value[position].avatarUrl
            holder.binding.title.text = model.items.value[position].login
            holder.binding.subtitle.text = model.items.value[position].type
            holder.binding.imageView.load(model.items.value[position].avatarUrl)
            holder.binding.root.setOnClickListener {
                findNavController().navigate(SearchOrgsFragmentDirections.actionShowTopRepos(model.items.value[position]))
            }
        }
    }

    class OrgHolder(val binding: ItemOrgBinding) : RecyclerView.ViewHolder(binding.root)

    class Factory(val useCase: SearchOrgsUseCase) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SearchOrgsViewModel(useCase) as T
        }
    }
}

