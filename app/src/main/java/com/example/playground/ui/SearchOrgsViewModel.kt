package com.example.playground.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.playground.core.MyIdlingResource
import com.example.playground.data.github.SearchOrgsUseCase
import com.example.playground.data.github.UserMatch
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber
import java.lang.Exception

@ExperimentalCoroutinesApi
class SearchOrgsViewModel(val useCase: SearchOrgsUseCase) : ViewModel() {

    private val _items = MutableStateFlow(emptyList<UserMatch>())
    val items: StateFlow<List<UserMatch>> get() = _items
    private var job: Job? = null

    fun onTermEntered(orgName: String) {
        job?.cancel()
        MyIdlingResource.increment()
        job = viewModelScope.launch(IO) {
            try {
                _items.value = useCase.queryOrganizations(orgName)
            } catch(e: Exception) {
                Timber.d("error ${e.message}")
            }
        }
        job?.invokeOnCompletion {
            MyIdlingResource.decrement()
        }
    }
}