package com.example.playground.ui

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.coroutineScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.playground.data.github.GitHubAPI
import com.example.playground.data.github.GithubRepository
import com.example.playground.data.github.QueryOrgTopReposUseCase
import com.example.playground.databinding.FragmentTopOrgReposBinding
import com.example.playground.databinding.ItemRepoBinding
import com.google.android.material.progressindicator.ProgressIndicator
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber

@ExperimentalCoroutinesApi
class TopOrgReposFragment : Fragment() {
    lateinit var binding: FragmentTopOrgReposBinding
    lateinit var model: TopOrgReposViewModel
    val args: TopOrgReposFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTopOrgReposBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = ViewModelProvider(this, Factory(args.org.login)).get(TopOrgReposViewModel::class.java)
        binding.orgName.text = args.org.login
        binding.imageView.load(args.org.avatarUrl)
        binding.recyclerView.adapter = MyAdapter()
        binding.progress.growMode = ProgressIndicator.GROW_MODE_INCOMING
        viewLifecycleOwner.lifecycle.coroutineScope.launchWhenResumed {
            model.items.collect {
                Timber.d("loaded")
                (binding.recyclerView.adapter as MyAdapter).notifyDataSetChanged()
                binding.progress.hide()
            }
        }
    }

    inner class MyAdapter: RecyclerView.Adapter<RepoHolder>() {

        override fun getItemCount(): Int {
            return model.items.value.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoHolder {
            return RepoHolder(ItemRepoBinding.inflate(layoutInflater, parent, false))
        }

        override fun onBindViewHolder(holder: RepoHolder, position: Int) {
            holder.binding.title.text = model.items.value[position].name
            holder.binding.subtitle.text = model.items.value[position].description
            holder.binding.root.setOnClickListener {
                val builder = CustomTabsIntent.Builder()
                val customTabsIntent = builder.build()
                customTabsIntent.launchUrl(this@TopOrgReposFragment.requireContext(),
                    Uri.parse(model.items.value[position].htmlUrl))
            }
        }
    }

    class RepoHolder(val binding: ItemRepoBinding) : RecyclerView.ViewHolder(binding.root) {
        init {

        }
    }

    class Factory(val orgName: String) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.HEADERS)
            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()

            val service = Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(MoshiConverterFactory.create())
                .client(client)
                .build()
                .create(GitHubAPI::class.java)

            return TopOrgReposViewModel(orgName, QueryOrgTopReposUseCase(GithubRepository(service))) as T
        }
    }
}