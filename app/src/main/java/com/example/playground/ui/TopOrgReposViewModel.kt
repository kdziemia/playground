package com.example.playground.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.playground.core.MyIdlingResource
import com.example.playground.data.github.OrganizationMatch
import com.example.playground.data.github.QueryOrgTopReposUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import timber.log.Timber

@ExperimentalCoroutinesApi
class TopOrgReposViewModel(val orgName: String, val useCase: QueryOrgTopReposUseCase): ViewModel() {
    private val _items = MutableStateFlow(emptyList<OrganizationMatch>())
    val items: StateFlow<List<OrganizationMatch>> get() = _items
    private var job: Job? = null

    init {
        load()
    }

    fun load() {
        job?.cancel()
        MyIdlingResource.increment()
        job = viewModelScope.launch {
            Timber.d("launching coroutine ${Thread.currentThread()}")
            try {
                _items.value = useCase.query(orgName)
            } catch(e: Exception) {
                Timber.d("error ${e.message}")
            }
        }
        job?.invokeOnCompletion {
            MyIdlingResource.decrement()
            Timber.d("invoke on completion ${it?.message} ${Thread.currentThread()}")
        }
    }
}