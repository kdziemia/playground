package com.example.playground.data.github

import java.net.URLEncoder
import javax.inject.Inject

class GithubRepository @Inject constructor(private val api: GitHubAPI) {

    suspend fun queryOrganizations(orgName: String) : List<UserMatch> {
        return this.api.searchUsers("${URLEncoder.encode(orgName, "utf-8")}+type:org").items
    }

    suspend fun queryTopRepositories(orgName: String) : List<OrganizationMatch> {
        return this.api.searchRepositories("org:${URLEncoder.encode(orgName, "utf-8")}").items
    }
}