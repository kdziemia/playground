package com.example.playground.data.github

import javax.inject.Inject

class QueryOrgTopReposUseCase@Inject constructor(private val repository: GithubRepository) {
    suspend fun query(orgName: String) : List<OrganizationMatch> {
        return repository.queryTopRepositories(orgName)
            .sortedByDescending { it.stargazersCount }
    }
}