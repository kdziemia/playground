package com.example.playground.data.github

import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface GitHubAPI {
    @Headers("Accept: application/vnd.github.v3+json")
    @GET("search/users")
    suspend fun searchUsers(@Query("q", encoded=true) query: String): SearchUsersResponse

    @Headers("Accept: application/vnd.github.v3+json")
    @GET("search/repositories")
    suspend fun searchRepositories(@Query("q", encoded=true) query: String): SearchRepositoriesResponse
}
