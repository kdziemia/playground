package com.example.playground.data.github

import javax.inject.Inject

class SearchOrgsUseCase @Inject constructor(val repository: GithubRepository) {

    suspend fun queryOrganizations(orgName: String): List<UserMatch> {
        return repository.queryOrganizations(orgName)
    }
}