package com.example.playground

import com.example.playground.data.github.GitHubAPI
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class GitHubAPITest {

    val api: GitHubAPI by lazy {

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS)
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        Retrofit.Builder()
            .baseUrl("https://api.github.com/")
            .addConverterFactory(MoshiConverterFactory.create())
            .client(client)
            .build()
            .create(GitHubAPI::class.java)
    }

    @Test
    fun testSearchUsersEndpoint() {
        runBlocking {
            val result = api.searchUsers("google")
            assert(result.items.isNotEmpty())
        }
    }

    @Test
    fun testSearchRepositoriesEndpoint() {
        runBlocking {
            val result = api.searchRepositories("google")
            assert(result.items.isNotEmpty())
        }
    }
}