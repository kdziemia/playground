package com.example.playground.core

import androidx.test.espresso.idling.net.UriIdlingResource

object MyIdlingResource {

    fun increment() {
        // do nothing
    }

    fun decrement() {
        // do nothing
    }
}