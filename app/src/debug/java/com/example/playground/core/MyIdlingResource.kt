package com.example.playground.core

import androidx.test.espresso.idling.net.UriIdlingResource
import java.lang.IllegalStateException

object MyIdlingResource {
    internal val resource = UriIdlingResource("GLOBAL", 1000)

    fun increment() {
        resource.beginLoad("")
    }

    fun decrement() {
        if (resource.isIdleNow) throw IllegalStateException()
        resource.endLoad("")
    }
}